# Requirements
The easiest way to manage the requirements for this project is using `poetry`.
If you don't already have it installed, run:

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

Then inside the directory of this project, run

```bash
poetry install
```

# Running
```bash
poetry run python3 iosfw/iosfw.py --help
```
